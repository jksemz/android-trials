package com.example.jethro.tutorial2;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener {

    private Button myButton;
    private Button myButton2;
    private TextView myText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myButton = (Button) this.findViewById(R.id.myButton);
        myButton = (Button) this.findViewById(R.id.myButton2);
        myText = (TextView) this.findViewById(R.id.myLabel);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.myButton:
                myText.setText("You click 1");
                break;
            case R.id.myButton2:
                myText.setText("You click 2");
                break;
        }
    }
}
